/**
 * Determines whether meat temperature is high enough
 * @param {string} kind 
 * @param {number} internalTemp 
 * @param {string} doneness
 * @returns {boolean} isCooked
 */
const foodIsCooked = (kind, internalTemp, doneness) => {
 
   
  if (kind === 'chicken' && internalTemp >= 165) {
      console.log(`${kind} is done, eat up!`);
      doneness = 'done';
      return doneness;
  }
  else if (kind === 'chicken' && internalTemp <= 164){
      console.log(`${kind} is not cooked yet, put those back in oven.`);
      doneness = 'not done'
      return doneness;
  }
  else if (kind === 'beef' && internalTemp <= 124){
      console.log(`${kind} is not cooked yet, cook it again.`);
      doneness = 'not done yet'
      return doneness;
  }
  else if (kind === 'beef' && internalTemp <= 125){
      doneness = 'rare'
      console.log(`${kind} is ${doneness}, serve it!`);
      return doneness;
  }
  else if (kind === 'beef' && internalTemp <= 135){
      doneness = 'medium'
      console.log(`${kind} is ${doneness}, serve it!`);
      return doneness;
  }
  else if (kind === 'beef' && internalTemp <= 155){
      doneness = 'well'
      console.log(`${kind} is ${doneness}, not fully cooked!`);
      return doneness;
  }
  else if (kind === 'beef' && internalTemp >= 155) {
      console.log(`${kind} is over cooked.`);
      doneness = 'Burned'
      return doneness;
  }
  console.log(`${doneness}`);
  return doneness;
};



// Test function
console.log(foodIsCooked('chicken', 90)); // should be false
console.log(foodIsCooked('chicken', 190)); // should be true
console.log(foodIsCooked('beef', 138, 'well')); // should be false
console.log(foodIsCooked('beef', 138, 'medium')); // should be true
console.log(foodIsCooked('beef', 138, 'rare')); // should be true