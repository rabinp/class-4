/**
 * Returns an array of 52 Cards
 * @returns {Array} deck - a deck of cards
 */
// const getDeck = () => {

// }

const getDeck = () => {
    let deck = [];
    let suits = ["Hearts", "Clubs", "Daimonds", "Spades"]
    let faces = ["Jack", "Queen", "King", "Ace"]
    
    for(let i=0; i <suits.length; i++){

        for (let j=2; j < 10; j++){
            let newCard = {
                val: j,
                displayVal: j.toString(),
                suits: suits[i]
            }
            console.log("the new card is:" ,newCard)
            deck.push(newCard)
        }
    
    for(let face = 0; face < faces.length; face++){
        let val = faces[face]=== "Ace" ? 11 : 10
        let newCard = {
            val,
            displayVal: faces[face],
            suit: suits[i]
        }
        console.log("the new card is:" ,newCard)
        deck.push(newCard)
    }
    console.log("deck so far", deck);
    return deck
}
}

// CHECKS
const deck = getDeck();
console.log(`Deck length equals 52? ${deck.length === 52}`);


const randomCard = deck[Math.floor(Math.random() * 52)];

const cardHasVal = randomCard && randomCard.val && typeof randomCard.val === 'number';
console.log(`Random card has val? ${cardHasVal}`);

const cardHasSuit = randomCard && randomCard.suit && typeof randomCard.suit === 'string';
console.log(`Random card has suit? ${cardHasSuit}`);

const cardHasDisplayVal = randomCard &&
  randomCard.displayVal &&
  typeof randomCard.displayVal === 'string';
console.log(`Random card has display value? ${cardHasDisplayVal}`);