const blackjackDeck = getDeck();

/**
 * Represents a card player (including dealer).
 * @constructor
 * @param {string} name - The name of the player
 */
class CardPlayer {
  constructor(name) {
    this.name = name
    this.hand = []
  }
  drawCard() {
    const newCard = blackjackDeck[Math.floor(Math.random() * blackjackDeck.length + 1)]
    console.log(`${this.name} drew a ${newCard.displayVal} of ${newCard.suit}`)
    this.hand.push(newCard)
  }

}; //TODO

// CREATE TWO NEW CardPlayers
const dealer = new CardPlayer("Frank"); // TODO
const player = new CardPlayer("Stetson"); // TODO



/**
 * Calculates the score of a Blackjack hand
 * @param {Array} hand - Array of card objects with val, displayVal, suit properties
 * @returns {Object} blackJackScore
 * @returns {number} blackJackScore.total
 * @returns {boolean} blackJackScore.isSoft
 */
const calcPoints = (hand) => {
  // CREATE FUNCTION HERE
  let blackJackScore = {
    total: 0,
    isSoft: false
  };
  
  hand.forEach(card => {
    
    if (card.displayVal === "Ace" && blackJackScore.total > 11){
      blackJackScore.total +=1;
      // console.log("log an Ace as 1")
    } else if (card.displayVal === "Ace") {
      // console.log("log an Ace as 11")
      blackJackScore.isSoft = true;
      blackJackScore.total += card.val;
    } else {
      // console.log("log a card")
      blackJackScore.total += card.val;

    } 
  });
  
  console.log(blackJackScore);

  return blackJackScore;

};

// calcPoints([ 
//   { val: 2, displayVal: '2', suit: 'spades' },
//   { val: 5, displayVal: '9', suit: 'spades' },
//   { val: 11, displayVal: '3', suit: 'spades' },
//   { val: 11, displayVal: 'Ace', suit: 'spades' },
// ]);

/**
 * Determines whether the dealer should draw another card.
 * 
 * @param {Array} dealerHand Array of card objects with val, displayVal, suit properties
 * @returns {boolean} whether dealer should draw another card
 */
const dealerShouldDraw = (dealerHand) => {
  // CREATE FUNCTION HERE
  let hit = false;
  let hand = calcPoints(dealerHand);

  if (hand.score <= 16 || (hand.score === 17 && hand.isSoft)){
    hit = true;
  }

  return hit;
};

/**
 * Determines the winner if both player and dealer stand
 * @param {number} playerScore 
 * @param {number} dealerScore 
 * @returns {string} Shows the player's score, the dealer's score, and who wins
 */
const determineWinner = (playerScore, dealerScore) => {
  // CREATE FUNCTION HERE
    //if(playerScore < 21 && dealerScore < 21) 
    {
    return `Player scores ${playerScore} and Dealer has ${dealerScore}`
  }

}

/**
 * Creates user prompt to ask if they'd like to draw a card
 * @param {number} count 
 * @param {string} dealerCard 
 */
const getMessage = (count, dealerCard) => {
  return `Dealer showing ${dealerCard.displayVal}, your count is ${count}.  Draw card?`
}

/**
 * Logs the player's hand to the console
 * @param {CardPlayer} player 
 */
const showHand = (player) => {
  const displayHand = player.hand.map((card) => card.displayVal);
  //debugger;
  console.log(`${player.name}'s hand is ${displayHand.join(', ')} (${calcPoints(player.hand).total})`);
}

/**
 * Runs Blackjack Game
 */
const startGame = function() {
  player.drawCard();
  dealer.drawCard();
  player.drawCard();
  dealer.drawCard();

  let playerScore = calcPoints(player.hand).total;
  showHand(player);
  while (playerScore < 21 && confirm(getMessage(playerScore, dealer.hand[0]))) {
    player.drawCard();
    playerScore = calcPoints(player.hand).total;
    showHand(player);
  }
  if (playerScore > 21) {
    return 'You went over 21 - you lose!';
  }
  console.log(`Player stands at ${playerScore}`);

  let dealerScore = calcPoints(dealer.hand).total;
  while (dealerScore < 21 && dealerShouldDraw(dealer.hand)) {
    dealer.drawCard();
    dealerScore = calcPoints(dealer.hand).total;
    showHand(dealer);
  }
  if (dealerScore > 21) {
    return 'Dealer went over 21 - you win!';
  }
  console.log(`Dealer stands at ${dealerScore}`);

  return determineWinner(playerScore, dealerScore);
}
 console.log(startGame());